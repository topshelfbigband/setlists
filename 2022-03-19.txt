Yokota/DuBois Wedding - FINAL
Lucie Stern Community Center at 1305 Middlefield Road, Palo Alto, CA

band plays ~6:45 until ~8:30

001 In the Mood
613 Pick up the Pieces
556 Sway (Genevieve vocal)
640 Soul Bossa
371 Summer Wind (Stan Vocal)
164 The Queen Bee
090 Sing, Sing, Sing
550 Strangers in the Night (Masako vocal)
365 On the Street Where You Live

break

559 Come Fly With Me (Stan Vocal)
583 The Way You Look Tonight (Stan vocal)
641 Son of a Preacher Man (Genevieve vocal)
042 Walkin’ My Baby Back Home (Clifford vocal)
643 Peter Gunn
596 Can't Take My Eyes Off of You (Ricky vocal)
352 It’s Oh, So Nice
041 Fly Me to the Moon (Stan Vocal)
013 Opus #1


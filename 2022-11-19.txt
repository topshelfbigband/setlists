Tabard Theatre Big Band Karaoke Party - FINAL
2022-11-19

=SET 1=

 119  Time After Time
   ?  ~8 karaoke songs
 653  Feeling Good

=SET 2=

 876  Every Summer Night
   ?  ~8 karaoke songs
 613  Pick up the Pieces

=SET 3=

 542  Sesame Street
   ?  ~8  karaoke songs
 535  The Jazz Police
  13  Opus #1 (encore)

Karaoke Song List:

  41  Fly me to the Moon 
  42  Walkin My Baby Back Home 
 300  Cheek to Cheek 
 312  Fever 
 371  Summer Wind 
 373  The Best is Yet to Come 
 432  You Make Me Feel So Young
 436  Too Darn Hot 
 438  Witchcraft
 494  It’s all Right With Me 
 500  A Tisket, A Tasket
 529  It Happened in Monterey 
 532  I’ve Got You Under My Skin
 545  New York, New York
 548  Paper Moon
 549  My Way
 550  Strangers in the Night
 551  Beyond the Sea
 556  Sway
 559  Come Fly With Me
 560  Time After Time
 561  Blue Skies
 563  Ain’t Misbehavin
 578  L-O-V-E
 579  Moondance (Michael Buble)
 582  Mack the Knife (vocal, Bobby Darin)
 596  Can’t Take My Eyes Off of You


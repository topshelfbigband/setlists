Sunnyvale Senior Center - FINAL
Wednesday, May 31, 2023; 6-8pm; 550 E. Remington Drive, Sunnyvale

=set 1=

365  On the Street Where You Live
350  Have You Met Miss Jones?
697  Straighten Up and Fly Right    (Genevieve vocal)
609  Stompin at the Savoy
663  Sentimental Journey            (Masako vocal)
660  Just Friends
693  Orange Colored Sky             (Genevieve vocal)
686  Eleanor Rigby

=set 2=

025  When You're Smiling
556  Sway                           (Genevieve vocal)
488  Out of Nowhere
533  Count Bubba
393  I Believe in You and Me        (Ricky vocal)
685  Charade
696  I Love Being Here With You     (Masako vocal)
593  My Way (key of Bb)             (Masako vocal)
425  Ya Gotta Try
013  Opus #1                        (encore)


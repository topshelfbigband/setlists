San Carlos Farmers' Market
2018-06-09

     18 Moten Swing
    433 Almost Like Being In Love (vocal)
    305 It's About Swing
    330 Love For Sale
    375 Skylark - Gm (vocal)
    522 Cherokee
    304 Lover's Leap
    430 920 Special
    498 Summer Samba
    494 It's All Right With Me (vocal) (arr Trevor Vincent, Ella Fitzgerald)

    525 Shorty George
    436 Too Darn Hot (vocal)
    513 God Bless the Child
    500 A Tisket A Tasket (vocal)
    520 I Could have Danced\
     23 I Just Found Out About Love (vocal)
     71 Big Noise From Winnetka
    302 Havana
    434 'T Ain't Whatcha Do (vocal)

    352 It's Oh, So Nice
    368 Autumn Leaves (Ted Heath)
    393 I Believe in You and Me (vocal)
    394 Springtime
    381 Begin The Beguine
    433 Almost Like Being In Love (vocal)
    423 September Song (Ted Heath)
    337 Rent Party
    370 Bare Necessities
    436 Too Darn Hot (vocal)

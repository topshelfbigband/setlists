FINAL #2 SETLIST

Tabard Theatre
2021-08-14: Tabard @ 5:30pm

Set 1

 119 Time After Time
 365 On the Street Where You Live
 572 I Get a Kick Out of You (Stan vocal)
 375 Skylark (Masako vocal)
 253 Pennsylvania 6-5000
 371 Summer Wind (Stan vocal)
 302 Havana
    solos @ rehearsal D: Joe first time, Dave Schwartz second time
 541 Cute
 313 Over the Rainbow (Masako vocal)
 301 All of Me (Dave Schwartz feature)
    bars 41-73 two times:
    Dave first time [NO backgrounds]
    Clifford second time [YES backgrounds]
 559 Come Fly With Me (Stan vocal)

Set 2

 594 Almost Like Being In Love
 368 Autumn Leaves
 591 A Time For Love (ballad, Jason feature)
 300 Cheek to Cheek (Stan vocal)
 573 Learning The Blues (Stan vocal)
 293 Bone Voyage
 581 When You Wish Upon A Star (Masako vocal)
 352 It's Oh, So Nice
 593 My Way (Masako vocal)
 423 September Song
 013 Opus #1
    Have this ready immediately; start this before applause dies from previous chart
    Vamp first 8 bars of G

Horeff/Domrose Wedding, March 23, 2024 - FINAL
6:00-7:30pm, Mountain Winery, 14831 Pierce Rd, Saratoga

=set 1=

119  Time After Time
682  Lullaby of the Leaves
352  It’s Oh, So Nice
573  Learning the Blues         (Stan vocal)
279  They Didn't Believe Me
639  Fun Time
644  The Way You Look Tonight   (Stan vocal)
708  Scott's Place
368  Autumn Leaves
025  When You're Smiling

=set 2=

598  April in Paris
567  Clip Joint Calamity
724  The Tender Trap            (Stan vocal)
650  Bob Omb Battlefield
640  Soul Bossa
539  Splanky
613  Pick up the Pieces
167  Wind Machine

Alternates:

095  Who's Sorry Now?
164  The Queen Bee
660  Just Friends
018  Moten Swing

Possible Tacia vocals:

041  Fly me to the Moon
561  Blue Skies

Requests:

559  Come Fly With Me (Stan vocal)
551  Beyond the Sea (Stan vocal)
578  L-O-V-E (vocal)
498  Summer Samba


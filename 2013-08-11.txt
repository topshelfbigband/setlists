San Jose Jazz Festival
2013-08-11

 95 - Who's Sorry Now? - 155
  9 - Clear Day
 66 - Someone to Watch Over Me
 71 - Big Noise from Winnetka - 175
 18 - Moten Swing - 135-140
 50 - Jalousie
 72 - Sun Valley Jump  - 160
 43 - East of the Sun
 11 - Little Brown Jug - 145
  6 - Getting Sentimental Over You
 32 - That Old Black Magic - 130
233 - Cottontail
266 - I Hadn't Anyone
  2 - Brazil
270 - American Patrol - 175
 94 - As Long as I Live
269 - You're the Cream in My Coffee
 35 - Basie Straight Ahead - 180
 86 - After the Lovin'
 48 - Hoop de Doo
 25 - When You're Smiling
113 - Chicago
 13 - Opus One
188 - Sweet Georgia Brown

above 90 minutes = 3.75 mins per
